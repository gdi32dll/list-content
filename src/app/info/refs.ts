import { Type } from '@angular/core';

abstract class ChangeDetectorRef {
    abstract markForCheck(): void;
    abstract detach(): void;
    abstract detectChanges(): void;
    abstract checkNoChanges(): void;
    abstract reattach(): void;
  }

abstract class ViewRef extends ChangeDetectorRef {
    abstract destroyed: boolean;
    abstract destroy(): void;
    abstract onDestroy(callback: () => void): any;
 }

abstract class EmbeddedViewRef<C> extends ViewRef {
    abstract context: C;
    abstract rootNodes: any[];
 }

abstract class ComponentRef<T> { }

abstract class ElementRef {}

interface ApplicationRef {
    componentTypes: Type<any>[];
    components: ComponentRef<any>[];
    attachView(viewRef: ViewRef);
    detachView(viewRef: ViewRef);
}

abstract class TemplateRef<C> {
    abstract elementRef: ElementRef;
    abstract createEmbeddedView(context: C): EmbeddedViewRef<C>;
  }
