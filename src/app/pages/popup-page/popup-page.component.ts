import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-popup-page',
  templateUrl: './popup-page.component.html',
  styleUrls: ['./popup-page.component.less']
})
export class PopupPageComponent implements OnInit {

  visible = false;

  constructor() { }

  ngOnInit() {
  }

  open() {
    this.visible = true;
  }

}
