import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopupPageComponent } from './popup-page.component';
import { PopupModule } from 'src/app/components/popup/popup.module';

@NgModule({
  declarations: [PopupPageComponent],
  exports: [PopupPageComponent],
  imports: [
    CommonModule,
    PopupModule,
    PopupModule
  ]
})
export class PopupPageModule { }
