import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AboutPageModule } from './about-page/about-page.module';
import { ListPageModule } from './list-page/list-page.module';
import { ListPageComponent } from './list-page/list-page.component';
import { AboutPageComponent } from './about-page/about-page.component';
import { PopupPageComponent } from './popup-page/popup-page.component';
import { PopupPageModule } from './popup-page/popup-page.module';

const routes: Routes = [
  {
    path: 'list',
    component: ListPageComponent
  },
  {
    path: 'popup',
    component: PopupPageComponent
  },
  {
    path: 'about',
    component: AboutPageComponent
  },
  {
    path: '**',
    redirectTo: 'list'
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes),
    CommonModule,
    AboutPageModule,
    PopupPageModule,
    ListPageModule
  ],
  exports: [RouterModule]
})
export class RoutingModule { }
