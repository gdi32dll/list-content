import { Component, OnInit } from '@angular/core';
import { ListPageService } from './list-page.service';

@Component({
  selector: 'app-list-page',
  templateUrl: './list-page.component.html',
  styleUrls: ['./list-page.component.less']
})
export class ListPageComponent implements OnInit {

  constructor(public service: ListPageService) { }

  ngOnInit() {
    this.service.loadAll();
  }

}
