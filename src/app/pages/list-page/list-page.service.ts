import { Injectable } from '@angular/core';
import { Item } from 'src/app/components/list/list.models';
import { BehaviorSubject } from 'rxjs';
import { delay, tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ListPageService {

    temp: Item[] =
    [
        {type: 'info', title: 'Response 300', content: 'Redirect route'},
        {type: 'success', title: 'Response 202', content: 'No content required'},
        {type: 'info', title: 'Response 304', content: 'Permanent redirect'},
        {type: 'success', title: 'Response 200', content: 'Succecc get data'},
        {type: 'error', title: 'Response 500', content: 'Server error!'}
    ];

    sourceSubject = new BehaviorSubject<Item[]>([]);

    get dataSource() {
        return this.sourceSubject;
    }

    loadAll() {
        this.sourceSubject.next([...this.temp]);
    }

    add() {
        this.temp.push({type: 'info', title: 'test', content: 'test content'});
        this.loadAll();
    }
}
