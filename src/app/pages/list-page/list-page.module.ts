import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListPageComponent } from './list-page.component';
import { ListModule } from 'src/app/components/list/list.module';
import { ListPageService } from './list-page.service';



@NgModule({
  declarations: [ListPageComponent],
  exports: [ListPageComponent],
  providers: [ListPageService],
  imports: [
    ListModule,
    CommonModule
  ]
})
export class ListPageModule { }
