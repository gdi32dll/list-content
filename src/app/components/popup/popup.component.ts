import { Component, OnInit, Input, Output,
  EventEmitter, ViewChild, TemplateRef, ApplicationRef,
  AfterContentChecked, ViewRef, ViewContainerRef } from '@angular/core';
import { DialogService } from './dialog.service';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.less']
})
export class PopupComponent implements OnInit, AfterContentChecked {

  @ViewChild('popup', {static: true}) popup: TemplateRef<any>;

  @Input() visible = false;
  @Output() visibleChange = new EventEmitter<boolean>();

  id = Symbol('popup id');
  created = false;
  private view: ViewRef;
  constructor(
    private container: ViewContainerRef,
    private service: DialogService) { }

  ngOnInit() {

  }

  ngAfterContentChecked(): void {
    this.show();
    this.hide();
  }

  show() {
    if (this.visible && !this.created) {
      if (!this.view) {
        console.log('Open create');
        this.container.createEmbeddedView(this.popup);
        this.service.attach(this);
      } else {
        console.log('Open insert');
        this.container.insert(this.view, 0);
      }
      this.created = true;
    }
  }

  hide() {
    if (!this.visible && this.created) {
      console.log('close');
      this.view = this.container.detach(0);
      this.service.detach(this);
      this.created = false;
    }
  }

  close() {
    this.visible = false;
    this.visibleChange.next(false);
  }
}
