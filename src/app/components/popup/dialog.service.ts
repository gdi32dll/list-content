import { Injectable } from '@angular/core';
import { PopupComponent } from './popup.component';
import { PopupPageModule } from 'src/app/pages/popup-page/popup-page.module';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  private components: PopupComponent[] = [];
  private current: PopupComponent;
  constructor() { }

  open() {

    this.setCurrent();
    this.components.forEach(c => c.hide());
  }

  close() {

    if (this.current) {
      this.current.show();
    }
  }

  attach(component: PopupComponent) {
    this.components.push(component);
  }

  detach(component: PopupComponent) {
    this.components = this.components.filter(c => c.id !== component.id);
  }

  private setCurrent() {
    this.current = this.components.filter(c => c.visible)[0] || null;
  }
}
