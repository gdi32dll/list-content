import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopupComponent } from './popup.component';
import { DialogComponent } from './dialog/dialog.component';
import { DialogService } from './dialog.service';

@NgModule({
  declarations: [PopupComponent, DialogComponent],
  exports: [PopupComponent],
  providers: [DialogService],
  imports: [
    CommonModule
  ]
})
export class PopupModule { }
