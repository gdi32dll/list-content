import { Directive, Input, TemplateRef, OnInit, Renderer2, ElementRef } from '@angular/core';

@Directive({
  selector: '[appListItem]'
})
export class ListItemDirective implements OnInit {

  @Input('appListItemType') type: string;

  constructor(
    public template: TemplateRef<any>
    ) { }

  ngOnInit(): void {
  }
}
