import {
  Component, Input, ContentChildren, QueryList,
  AfterContentChecked, OnChanges, SimpleChanges
} from '@angular/core';
import { Item, ItemContext } from '../list.models';
import { ListItemDirective } from '../list-item.directive';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.less']
})
export class ListComponent implements AfterContentChecked, OnChanges {

  @Input() dataSource: Item[];

  cached = false;
  context: ItemContext[] = [];

  @ContentChildren(ListItemDirective, { descendants: true }) templateDirectives: QueryList<ListItemDirective>;

  private itemsDirectiveByType = new Map<string, ListItemDirective>();

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.dataSource) {
      this.cached = true;
    }
  }

  ngAfterContentChecked(): void {
    if (this.cached) {
      this.cacheItems();
      this.createContext();
      this.cached = false;
    }
  }

  private cacheItems() {
    if (this.itemsDirectiveByType.size > 0) {
      return;
    }
    this.templateDirectives.forEach(itemDir => {
      if (this.itemsDirectiveByType.has(itemDir.type)) {
        throw new Error(`dublicated types ${itemDir.type}`);
      }
      this.itemsDirectiveByType.set(itemDir.type, itemDir);
    });
  }

  private createContext() {
      this.context = this.dataSource.map(x => ({
        context: {
          $implicit: x,
          className: `item item-${x.type}`
        },
        template: this.itemsDirectiveByType.get(x.type).template
      }));
  }

}
