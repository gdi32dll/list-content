import { TemplateRef } from '@angular/core';

export interface Item {
    type: string;
    title: string;
    content: string;
}

export interface ItemContext {
    context: {$implicit: Item};
    template: TemplateRef<any>;
}
